const Koa = require('koa')
const Router = require('koa-router')

const app = new Koa()
const router = new Router()

const logger = require('./logger')

let accountant = 0

router
  .get ('/', (ctx, next) => {
    ctx.body = {
      payload: {
        count: accountant
      }
    }
  })
  .get('/add', (ctx, next) => {
    ctx.body = {
      payload: {
        count: ++accountant
      }
    }
  })

app.use(router.routes())
app.use(router.allowedMethods({
  throw: true,
  notImplemented: () => new Boom.notImplemented(),
  methodNotAllowed: () => new Boom.methodNotAllowed()
}))

app.listen(3000, () => {
  console.log('Server Run... 🚀')
})

app.on('error', err => {
  logger.add(new winston.transports.Console({
    format: winston.format.simple()
  }))
})
